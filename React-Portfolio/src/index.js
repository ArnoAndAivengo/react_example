import React from 'react';
import ReactDOM from 'react-dom';

import './resources/css/index.css';
import './resources/css/default.css';
import './resources/css/fonts.css';
import './resources/css/layout.css';
import './resources/css/magnific-popup.css';
import './resources/css/media-queries.css';
import './resources/css/font-awesome/scss/font-awesome.scss';

import App from './App';
import * as serviceWorker from './utils/serviceWorker';

ReactDOM.render(<App />, document.getElementById('root'));

serviceWorker.unregister();
