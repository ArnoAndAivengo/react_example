import React  from 'react';
import { Header, About, Portfolio, Resume, Footer, ContactUs, Testimonials } from './components/Index';

import resumeData from './resources/store/resumeData';

function App () {
  return (
    <div className="App">
      <Header resumeData={resumeData}/>
      <About resumeData={resumeData}/>
      <Resume resumeData={resumeData}/>
      <Portfolio resumeData={resumeData}/>
      {/*<Testimonials resumeData={resumeData}/>*/}
      {/*<ContactUs resumeData={resumeData}/>*/}
      <Footer resumeData={resumeData}/>
    </div>
  )
}

export default App;