import React from 'react';

import './Blog.css';

class Blog extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            posts: [
                {id: 1, title: 'Привет, мир', content: 'Добро пожаловать в документацию React!', link: 'http://localhost:8081'},
                {id: 2, title: 'Установка', content: 'React можно установить из npm.', link: 'http://localhost:8082'}
            ]
        }
    }

    Blog(props) {
        const sidebar = (
            <ul>
                {props.posts.map((post) =>
                    <li key={post.id}>
                        <a href={post.link}>{post.title}</a>
                    </li>
                )}
            </ul>
        );
        const content = props.posts.map((post) =>
            <div key={post.id}>
                <h3>{post.title}</h3>
                <p>{post.content}</p>
            </div>
        );
        return (
            <div>
                {sidebar}
                <hr />
                {content}
            </div>
        );
    }

    render() {
        return (
            <this.Blog posts={this.state.posts} />
        );
    }
}

export default Blog;
