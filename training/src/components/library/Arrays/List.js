import React from 'react';

class List extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            items: [
                "Home",
                "Resume",
                "Contacts",
                "About",
                "Social"
            ],
            link: [
                "localhost:8080/",
                "localhost:8081/",
                "localhost:8082/",
                "localhost:8083",
                "localhost:8084/"
            ]
        }
    }

    listItems(props) {

        const items = props.list.items;
        const link = props.list.link;

        const listItems = items.map((items, key) =>
            <li key={items.toString()}>
                <a href={link[key]}>{items}</a>
            </li>
        );

        return (
            <ul className={"menu_items"}>{listItems}</ul>
        );
    };

    render() {
        return (
            <this.listItems list={this.state} />
        );
    }
}

export default List;
