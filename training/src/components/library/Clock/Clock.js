import React from 'react';

class Clock extends React.Component {

    constructor(props) {
        super(props);
        this.state = {date: new Date()};
    }

    // lifecycle methods
    // Метод componentDidMount() запускается после того, как компонент отрендерился в DOM — здесь мы и установим таймер:
    componentDidMount() {
        this.timerID = setInterval(
            () =>this.tick(),
            1000
        )
    }

    componentWillUnmount() {
        clearInterval(this.timerID)
    }

    tick() {
        this.setState({ // планирует обновление внутреннего состояния компонента:
            date: new Date()
        })
    }

  render() {
    return (
        <div>
          <h1>Привет, мир!</h1>
          <h1>Сейчас {this.state.date.toLocaleTimeString()}</h1>
        </div>
    );
  }
}

export default Clock;
