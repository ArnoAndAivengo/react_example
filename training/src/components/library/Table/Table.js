import React from 'react';
import './Table.css';
import 'react-table/react-table.css';

class Table extends React.Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
        fetch("src/resources/json/stations.json")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        items: result.data
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    render() {
        const { error, isLoaded, items } = this.state;
        if (error) {
            return <div>Ошибка: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Загрузка...</div>;
        } else {
            return (
                <div>
                    <table>
                        <thead>
                        <tr>{items.map((item, i) => <TableHeader key={i} scope="col" data={item} />)}</tr>
                        </thead>
                        <tbody>
                            {items.map((item, i) => <TableRow key={i} data={item} />)}
                        </tbody>
                    </table>
                </div>
            );
        }
    }
}

class TableRow extends React.Component {
    render() {
        return (
            <tr className={"tr"}>
                <td className={"td"}>{this.props.data.id}</td>
                <td className={"td"}>{this.props.data.long_name}</td>
                <td className={"td"}>{this.props.data.road_id}</td>
            </tr>
        );
    }
}

class TableHeader extends React.Component {
    render() {
        return (
            <th>{this.props.data.long_name}</th>
        );
    }
}

export default Table;
