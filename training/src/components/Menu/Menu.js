import React from 'react';

import './Menu.css';

class Menu extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: []
        };
    }

    componentDidMount() {
        fetch("src/resources/json/menuItems.json")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        items: result.items
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    render() {
        const { error, isLoaded, items } = this.state;
        if (error) {
            return <div>Ошибка: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Загрузка...</div>;
        } else {
            return (
                <ul className={"menu"}>
                    {items.map(item => (
                        <li id={item.id} className={"menu_items"} key={item.items}>
                            <a href={item.link}>{item.items}</a>
                        </li>
                    ))}
                </ul>
            )
        }
    }
}

export default Menu;
