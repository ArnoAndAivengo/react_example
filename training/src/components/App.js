import React, { Component } from "react";
import Menu from './Menu/Menu';
import Blog from './blog/Blog';
import Main from './Main';
import Table from './library/Table/Table';
import '../styles/index.css';
import './App.css';

class App extends Component {
    render() {
        return (
            <Main />
        );
    }
}

export default App;