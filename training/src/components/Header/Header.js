import React, { Component } from "react";

import Menu from '../Menu/Menu';

// import '../styles/index.css';

class Header extends Component {
    render() {
        return (
            <header id={"Header"}>
                <Menu />
            </header>
        );
    }
}

export default Header;