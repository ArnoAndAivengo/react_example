import React, { useState, useCallback } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import LoginModal from './LoginModal'

const Header = () => {
    const [login] = useSelector(state => [state.login])
    const dispatch = useDispatch()
    const [loginModal, setLoginModal] = useState(false)
    const setCloseLogin = useCallback(() => {
        setLoginModal(false)
        document.body.style.overflow = 'auto'
    }, [])
    const setOpenLogin = useCallback(() => {
        setLoginModal(true)
        document.body.style.overflow = 'hidden'
    }, [])

    const logOut = () => {
        sessionStorage.removeItem('token')
        dispatch({ type: 'SET_LOGOUT' })
    }

    return (
        <React.Fragment>
            <header>
                <div className='navbar'>
                    <div className="container">
                        <div className="navbar-content">
                            <ul className="navbar-list">
                                {
                                    login ?
                                        <li className="navbar-item"
                                            onClick={logOut}
                                        >
                                            <a href="#" className="navbar-link">Выйти</a>
                                        </li>
                                        :
                                        <li className="navbar-item"
                                            onClick={setOpenLogin}
                                        >
                                            <a href="#" className="navbar-link">Авторизация</a>
                                        </li>
                                }
                            </ul>
                        </div>
                    </div>
                </div>
            </header>
            {
                loginModal &&
                <LoginModal
                    setCloseLogin={setCloseLogin}
                />
            }
        </React.Fragment>
    )
}

export default Header
