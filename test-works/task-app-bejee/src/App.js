import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux'

import Header from './components/Header'
import TaskCreate from './components/TaskCreate'
import TaskList from './components/TaskList'


function App() {
    const dispatch = useDispatch()

    useEffect(() => {
        sessionStorage.getItem('token') && dispatch({ type: 'SET_LOGIN' })
    }, [dispatch])

    return (
        <React.Fragment>
            <Header />
            <div className='container'>
                <div className="wrapper">
                    <div className="wrapper-taskcreate">
                        <TaskCreate />
                    </div>
                    <div className="wrapper-tasklistk" styles="width=100%">
                        <TaskList />
                    </div>
                </div>
            </div>
        </React.Fragment>

    )
}

export default App
