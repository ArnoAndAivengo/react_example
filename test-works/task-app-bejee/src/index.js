import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'

import store from './resources/store'
import App from './App'

import './resources/assets/scss/main.scss'

import * as serviceWorker from './utils/serviceWorker'

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>, document.getElementById('root'))

serviceWorker.unregister()
