import React, { Component } from 'react';
import { connect }  from 'react-redux';
import* as actions from '../actions'
import { bindActionCreators } from 'redux';

const Counter = ({vall, counter, inc, dec, rnd}) => {
    return (
        <div className="jumbotron">
            <h2>{counter}</h2>
            <h2>{vall}</h2>
            <button
                onClick={dec}
                className="btn btn-primary btn-lg">DEC</button>
            <button
                onClick={inc}
                className="btn btn-primary btn-lg">INC</button>
            <button
                onClick={rnd}
                className="btn btn-primary btn-lg">RND</button>
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        counter: state,
        vall: 23
    }
};

// const mapDispatchToProps = (dispatch) => {
//
//     return bindActionCreators(actions, dispatch);
    //
    // const { inc, dec, rnd } = bindActionCreators(actions, dispatch);
    //
    // return {
    //     inc,
    //     dec,
    //     rnd
    // }
// };

export default connect(mapStateToProps, actions)(Counter);