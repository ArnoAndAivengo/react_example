import React from 'react';
import ReactDom from 'react-dom';
import { createStore, bindActionCreators } from 'redux';
import { Provider } from 'react-redux';
import reducer from './reducer';
import App from './components/app';
// import Counter from './components/counter';
// import * as actions from './actions';

const store = createStore(reducer);
// const { dispatch } = store;

// const bindActionCreator = (creator, dispatch) => (...args) => {
//     dispatch(creator(...args));
// };

// const { inc, dec, rnd } = bindActionCreators(actions, dispatch);

// document.getElementById('inc').addEventListener('click', inc);
// document.getElementById('dec').addEventListener('click', dec);
// document.getElementById('rnd').addEventListener('click', () => {
//     const payload = Math.floor(Math.random() * 10);
//     rnd(payload)
// });


// const update = () => {
//     // document.getElementById('counter').innerHTML = store.getState();
ReactDom.render(
    // eslint-disable-next-line react/react-in-jsx-scope
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root'));
// };
// update();
// store.subscribe(update);