import React, { Component } from 'react';


import AppHeader from '../app-header'
import TodoList from '../todo-list'
import SearchPanel from '../search-panel'
import ItemStatusFilter from '../item-status-filter';
import ItemAddForm from '../item-add-form';

import './app.css'

export default class App extends Component {

    maxId = 100;

    state = {
        todoData:  [
            this.createTodoItem('Погулять'),
            this.createTodoItem('Покушать'),
            this.createTodoItem('Поспать'),
        ],
        term: ''
    };

    createTodoItem(label) {
        return {
            label,
            important: false,
            done: false,
            id: ++this.maxId
        }
    }

    onDelete = (id) => {
        this.setState(({ todoData }) => {
            const idx = todoData.findIndex((el) => el.id === id);

            const newArray = [
                ...todoData.slice(0, idx),
                ...todoData.slice(idx + 1)
            ];

            return {
                todoData: newArray
            }
        })
    };

    addItem = (text) => {

        const newItem = this.createTodoItem(text);


        this.setState(({ todoData }) => {

            const newArr = [
                ...todoData,
                    newItem
            ];

            return {
                todoData: newArr
            }

        });
    };

    toggleProperty(arr, id, propName) {

        const idx = arr.findIndex((item) => item.id === id);

        const oldItem = arr[idx];

        const item = {
            ...arr[idx],
            [propName]: !oldItem[propName]
        };

        return[
            ...arr.slice(0, idx),
            item,
            ...arr.slice(idx + 1)
        ];

    };

    onToggleImportant = (id) => {
        this.setState(({todoData}) => {
            return {
                todoData: this.toggleProperty(todoData, id, 'important')
            }
        });
    };

    onToggleDone = (id) => {

        this.setState(({todoData}) => {
            return {
                todoData: this.toggleProperty(todoData, id, 'done')
            }
     });
    };

    search(items, term)  {
        if (term.length === 0) {
            return items;
        }

        return items.filter((item) => {
            return item.label.toLowerCase().indexOf(term.toLowerCase()) > -1;
        })
    };

    onSearchChange = (term) => {
        this.setState({ term });
    }

    render() {

        const { todoData, term } = this.state;
        const visibleItems = this.search(todoData, term)
        const doneCount = todoData.filter((item) => item.done).length;
        const todoCount = todoData.length - doneCount;

        return (
            <div className="todo-app">
                <AppHeader toDo={todoCount} done={doneCount} />
                <div className="top=panel d-flex">
                    <SearchPanel onSearchChange={this.onSearchChange}/>
                    <ItemStatusFilter />
                </div>

                <TodoList
                    todos={visibleItems}
                    onToggleImportant={this.onToggleImportant}
                    onToggleDone={this.onToggleDone}
                    onDelete={ this.onDelete}
                />

                <ItemAddForm onItemAdded={this.addItem}/>
            </div>
        )
    }
};